import React, {Component} from 'react';
import styled from "styled-components";

const CalculatorScreen = styled.div`
    background: #273746;
    padding: 20px 20px 30px;
    
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    
    position: relative;
`;

const Calculation = styled.span`
    font-size: 18px;
    color: #AAB7B8
    max-width: 520px;
    flex: 0 1 100%;
`;

const CalculationResult = styled.span`
    font-size: 30px;
`;


class Screen extends Component {
    render() {
        const {calc, result} = this.props;
        return (
            <CalculatorScreen>
                <Calculation>{calc}</Calculation>
                <CalculationResult>{result}</CalculationResult>
            </CalculatorScreen>
        )
    }
}

export default Screen;