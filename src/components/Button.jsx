import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Btn = styled.button`
  background: transparent;
  border: 0;
  color: #fff;
  font-size: 30px;
  max-width: 85px;
  width: 100%;
  height: 65px;
  line-height: 65px;
`;

const BtnWide = styled.button`
  background: transparent;
  border: 0;
  color: #fff;
  font-size: 30px;
  max-width: 170px;
  width: 100%;
  height: 65px;
  line-height: 65px;
`;

class Button extends Component {
  getSymbol = () => {
    this.props.handleClick(this.props.symbol);
  };

  render() {
    const { width, symbol } = this.props;

    if (width === "wide") {
      return <BtnWide onClick={this.getSymbol}>{symbol}</BtnWide>;
    } else if (width === "narrow") {
      return (
        <Btn onClick={this.getSymbol}>
          {symbol === "*" ? "\t\u00D7" : symbol}
        </Btn>
      );
    }
  }
}

export default Button;

Button.propTypes = {
  type: PropTypes.string.isRequired,
  symbol: PropTypes.string.isRequired
};
