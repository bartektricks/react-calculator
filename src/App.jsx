import React, { Component } from "react";
import styled, { injectGlobal } from "styled-components";
import WebFont from "webfontloader";

import Screen from "./components/Screen";
import Button from "./components/Button";

WebFont.load({
  google: {
    families: ["Roboto:500", "sans-seriff"]
  }
});

injectGlobal`
    body {
        margin: 0;
        padding: 0;
        background: papayawhip;
        color: #fff;
        font-family: 'Roboto', sans-serif;
        letter-spacing: 1px;
    }
    
    #root {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        padding: 0 20px;
    }
`;

const Container = styled.div`
  border: 2px solid #273746;
  border-radius: 6px;
  max-width: 340px;
  width: 100%;
  overflow: hidden;
  background: #34495e;

  -webkit-box-shadow: 0px 10px 50px -12px rgba(52, 73, 94, 1);
  -moz-box-shadow: 0px 10px 50px -12px rgba(52, 73, 94, 1);
  box-shadow: 0px 10px 50px -12px rgba(52, 73, 94, 1);
`;

const Row = styled.div`
  display: flex;
`;

class App extends Component {
  state = {
    calculations: 0,
    result: 0
  };

  handleClick = value => {
    switch (value) {
      // Clears entire state
      case "CE": {
        this.clearState();
        break;
      }
      // Clears last typed element
      case "C": {
        this.setState({
            calculations: this.state.calculations.substring(0, this.state.calculations.length -1)
        });
        break;
      }
      // Returns result of the equation to the state
      case "=": {
        let result = eval(this.state.calculations);
        result = Math.round(result*1000)/1000;
        this.setState({
          calculations: result,
          result
        });
        break;
      }
      // Returns the equation to the state
      default: {
        this.calculate(value);
      }
    }
  };

  clearState = () => {
    this.setState({
      calculations: 0,
      result: 0
    });
  };

  calculate = value => {
    const isInteger = Number.isInteger(value);
    value = value.toString();
    //Checks if the calculations aren't empty / equal to 0 && the value is a number
    if (this.state.calculations === 0 && isInteger) {
      this.setState({
        calculations: value
      });
      //Checks if the result is equal to the calculations && if the value is a number
    } else if (this.state.calculations === this.state.result && isInteger) {
      this.setState({
        calculations: value,
        result: 0
      });
      //Pushes value to the calculations State
    } else {
      this.setState({
        calculations: (this.state.calculations += value)
      });
    }
  };

  render() {
    const { calculations, result } = this.state;
    return (
      <Container>
        <Screen calc={calculations} result={result} />
        <Row>
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="CE"
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="C"
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="%"
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="*"
          />
        </Row>
        <Row>
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={7}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={8}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={9}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="/"
          />
        </Row>
        <Row>
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={4}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={5}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={6}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="-"
          />
        </Row>
        <Row>
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={3}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={2}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol={1}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="+"
          />
        </Row>
        <Row>
          <Button
            handleClick={this.handleClick}
            width="wide"
            symbol={0}
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="."
          />
          <Button
            handleClick={this.handleClick}
            width="narrow"
            symbol="="
          />
        </Row>
      </Container>
    );
  }
}

export default App;
